#!/system/bin/sh
# preinstall
# 2015 neomode @ freaktab.com
if [ ! -e /data/.notfirstboot ]
then
    busybox touch /data/.notfirstboot
    
    if [ -e /system/preinstall/ ]
    then
        APKLIST=`ls /system/preinstall/*.apk`
        for INFILES in $APKLIST
        do
          pm install -r $INFILES
          busybox sleep 2m
	 	done
    fi
    
    if [ -e /mnt/external_sd/autoinstall/ ]
    then
        APKLIST=`ls /mnt/external_sd/autoinstall/*.apk`
        for INFILES in $APKLIST
        do
            pm install -r $INFILES
        done
    fi
	
    if [ -e /system/preinstall/recovery.img ]
    then
        if [ ! -e /mnt/external_sd/autoinstall/noflashrecovery ]
        then
            flash_image recovery /system/preinstall/recovery.img
            mount -o rw,remount /system
            mv /system/preinstall/recovery.img /system/preinstall/recovery.done

            mount -o ro,remount /system
            
            if [ -e /system/preinstall/cwm ]
            then
                cp -r /system/preinstall/cwm /mnt/external_sd/cwm
            fi
        fi
    fi
	if [ -e /system/preinstall/SuperSU.zip ]
	then
		cp /system/preinstall/SuperSU.zip /mnt/internal_sd/SuperSU.zip
		echo 'boot-recovery ' > /cache/recovery/command
		echo '--update_package=/mnt/internal_sd/SuperSU.zip' >> /cache/recovery/command
		reboot recovery
	fi
fi
exit
